#include "detpic32.h"

void configSwitch();
void configLeds();
char readInputs();
void printLeds(char value);

int main()
{
	configSwitch(); // configura os portos do DipSwitch
	configLeds();
	while(1){
		char value  = readInputs();
		printLeds(value);
	}
	return 0;
}

void configSwitch(){
	TRISE = (TRISE | 0xF0);
}

void configLeds(){
	TRISE = TRISE & 0xF0;
}

char readInputs(){
	return PORTE >> 4;
}

void printLeds(char value){
	LATE = value;
}
