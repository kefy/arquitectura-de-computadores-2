#include "detpic32.h"

// Funçoes ADC
void configADC(unsigned char port);
unsigned int analogRead(unsigned char port);

// Funções para LEDS
void configLeds();
void printLeds(char value);


//##################################################################################################
//						Configuração das portas Analógicas
#define		numSamples		8	// 8 samples
#define		operationMode	'P' // modo de operação por polling
//##################################################################################################

int main()
{
	configLeds();
	while(1){
		unsigned int value = analogRead(14);
		//convert to 4 bits for LEDS
		printStr("Value:  ");
		printInt(value,16);
		value = (value * 15)/(1023);
		printLeds(value);
	 }
	 
	return 0;
}

unsigned int analogRead(unsigned char port){
	configADC(14); // pode ser desnecessário caso seja configurado no início. Porém com esta maneira consigo ler várias portas analógicas ao sempre que invoco a função com o número da porta.
	AD1CON1bits.ASAM	=	1;
	while(IFS1bits.AD1IF	==	0);		// wait until conversion is done
	IFS1bits.AD1IF		=	0;			// reset AD1IF
	int *p = (int * )(&ADC1BUF0);
	int i = 0;
	unsigned int sum = 0;
	for(;i<numSamples;i++){
		sum = sum + p[i*4];
	}
	return sum/numSamples;
}

void configADC(unsigned char port){ // configura porto 14
	//desligar portas e configurar AN14
	TRISB = (TRISB & (0xFFFF ^ (0x01 <<port))) | (0x01 <<port); // set position " port " in TRISB to 1
	AD1PCFG = (AD1PCFG & (0xFFFF ^ (0x01 <<port)));	// set position " port " in AD1PCFGbits to 0
	putChar('\n');
//	printInt(AD1PCFG,2);


	AD1CON1bits.SSRC	= 7;
	AD1CON1bits.CLRASAM = 1;	// stops conversion;
	AD1CON3bits.SAMC = 16;
	AD1CON2bits.SMPI = numSamples-1;
	AD1CHSbits.CH0SA = port;
	AD1CON1bits.ON = 1;
	
	if(operationMode == 'I'){
		IPC6bits.AD1IP = 3;
		IEC1bits.AD1IE  = 1;
	}
}

void printLeds(char value){
	LATE = value;
}

void configLeds(){
	TRISE = TRISE & 0xF0;
}

