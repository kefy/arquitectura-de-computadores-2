#include "detpic32.h"

// Funçoes ADC
void configADC(unsigned char port);
unsigned int analogRead(unsigned char port);

// Funções para displays
void configSegments();
void send2Segments(unsigned char value);
void selectDisplay(unsigned int displ);
char toggle(int value);


// auxiliar
unsigned char toBCD(unsigned char value);

//##################################################################################################
//						Configuração das portas Analógicas
#define		numSamples		8	// 8 samples
#define		operationMode	'P' // modo de operação por polling
//##################################################################################################

int main()
{
	configSegments();
	int i = 0;
	unsigned int value = analogRead(14);
	while(1){
		
		// 10 ms
		while(readCoreTimer() < (20000000/100)); // 10 ms
		resetCoreTimer();
		
		if(++i == 2){				// 20 ms = 1/50
			value = analogRead(14);
			value = (value * 33)/(1023);
			value = toBCD(value);
			i = 0;
		}
		send2Segments(value);			
	 }	
	 
	return 0;
}

unsigned int analogRead(unsigned char port){
	
	if(operationMode=='P'){
		configADC(14); // pode ser desnecessário caso seja configurado no início. Porém com esta maneira consigo ler várias portas analógicas ao sempre que invoco a função com o número da porta.
		AD1CON1bits.ASAM	=	1;
		while(IFS1bits.AD1IF	==	0);		// wait until conversion is done
		IFS1bits.AD1IF		=	0;			// reset AD1IF
	}
	int *p = (int * )(&ADC1BUF0);
	int i = 0;
	unsigned int sum = 0;
	for(;i<numSamples;i++){
		sum = sum + p[i*4];
	}
	return sum/numSamples;
}

void configADC(unsigned char port){ // configura porto 14
	//desligar portas e configurar AN14
	TRISB = (TRISB & (0xFFFF ^ (0x01 <<port))) | (0x01 <<port); // set position " port " in TRISB to 1
	AD1PCFG = (AD1PCFG & (0xFFFF ^ (0x01 <<port)));	// set position " port " in AD1PCFGbits to 0
	AD1CON1bits.SSRC	= 7;
	AD1CON1bits.CLRASAM = 1;	// stops conversion;
	AD1CON3bits.SAMC = 16;
	AD1CON2bits.SMPI = numSamples-1;
	AD1CHSbits.CH0SA = port;
	AD1CON1bits.ON = 1;
	
	if(operationMode == 'I'){
		IPC6bits.AD1IP = 3;
		IEC1bits.AD1IE  = 1;
	}
}

void send2Segments(unsigned char value){
	static unsigned char disp = 0;
	static unsigned char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};

	selectDisplay(disp);
	LATB = ((LATB & 0xFF00) | (code[(value >> 4) & 0x0F] * disp + code[value & 0x0F] * !disp)); // (code[(value >> 4) & 0x0F] * disp + code[value & 0x0F] * !disp)
	disp = toggle(disp);
}

void selectDisplay(unsigned int disp){ // 0 => displ_low ; 1 displ_high
	LATBbits.LATB8 = !disp;
	LATBbits.LATB9 = disp;
}

char toggle(int value){
	return  ! value;
}

void configSegments(){ // configurar segmentos e selecção dos segmentos
	TRISB = TRISB & 0xFC00;
}

unsigned char toBCD(unsigned char value){
	return ((value / 10) << 4) + (value % 10);	
}
