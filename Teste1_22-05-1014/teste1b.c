#include "detpic32.h"

void configSwitch();
void configSegments();
char readInputs();
void send2Segments(unsigned char value);
void selectDisplay(unsigned int displ);
char toggle(int value);
void delay(unsigned int n_intervals);

int main()
{
	configSegments();
	configSwitch();
	while(1){
	
		char value  = readInputs();
		printInt(value,16);
		send2Segments(value);		
	}
	return 0;
}

void configSwitch(){
	TRISE = (TRISE | 0xF0);
}

void configSegments(){ // configurar segmentos e selecção dos segmentos
	TRISB = TRISB & 0xFC00;
}

char readInputs(){
	return PORTE >> 4;
}

void send2Segments(unsigned char value){
	static unsigned char disp = 0;
	static unsigned char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};

	selectDisplay(disp);
	LATB = ((LATB & 0xFF00) | (code[(value >> 4) & 0x0F] * disp + code[value & 0x0F] * !disp)); // (code[(value >> 4) & 0x0F] * disp + code[value & 0x0F] * !disp)
	disp = toggle(disp);
}

void selectDisplay(unsigned int disp){ // 0 => displ_low ; 1 displ_high
	LATBbits.LATB8 = !disp;
	LATBbits.LATB9 = disp;
}

char toggle(int value){
	return  ! value;
}

void delay(unsigned int n_intervals){
	volatile unsigned int i;
	
	for(; n_intervals != 0 ; n_intervals--){
		for(i = 5000; i != 0 ; i--);
	}
}
