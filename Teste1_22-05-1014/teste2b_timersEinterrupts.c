#include "detpic32.h"

// Funçoes ADC
void configADC(unsigned char port);
unsigned int analogRead(unsigned char port);

// Funções para displays
void configSegments();
void send2Segments(unsigned char value);
void selectDisplay(unsigned int displ);
char toggle(int value);

// configuração dos timers
void configTimer1(unsigned int pr1, unsigned int tckps);
void configTimer3(unsigned int pr3, unsigned int tckps);

// auxiliar
unsigned char toBCD(unsigned char value);

//##################################################################################################
//						Configuração das portas Analógicas
#define		numSamples		8	// 8 samples
#define		operationMode	'I' // modo de operação por interrupção
//##################################################################################################

// global variables

	volatile unsigned int value = 0;

int main()
{
	configSegments();
	configADC(14);
	configTimer1(49999,2);
	configTimer3(24999,3);
	EnableInterrupts();
	while(1);
	return 0;
}

void _int_(4) isr_T1(void){
	AD1CON1bits.ASAM	=	1;
	IFS0bits.T1IF		=	0;
}

void _int_(12) isr_T3(void){
	send2Segments(value);
	IFS0bits.T3IF		= 	0;
}

void _int_(27) isr_adc(void){
	value = toBCD((analogRead(14) * 33) / 1023);
	printStr("here");
	IFS1bits.AD1IF		= 	0;
}

void configTimer1(unsigned int pr1, unsigned int tckps){
	T1CONbits.TCKPS		=		tckps;
	PR1					=		pr1;
	TMR1				=		0;
	T1CONbits.ON		=		1;
	
	if (operationMode=='I'){
		IFS0bits.T1IF 	= 0;
		IPC1bits.T1IP 	= 3;
		IEC0bits.T1IE	= 1;
	}
}
void configTimer3(unsigned int pr3, unsigned int tckps){
	T3CONbits.TCKPS		=		tckps;
	PR3					=		pr3;
	TMR3				=		0;
	T3CONbits.ON		=		1;
	
	if (operationMode=='I'){
		IFS0bits.T3IF 	= 0;
		IPC3bits.T3IP 	= 3;
		IEC0bits.T3IE	= 1;
	}	
}

unsigned int analogRead(unsigned char port){
	
	if(operationMode=='P'){
		configADC(14); // pode ser desnecessário caso seja configurado no início. Porém com esta maneira consigo ler várias portas analógicas ao sempre que invoco a função com o número da porta.
		AD1CON1bits.ASAM	=	1;
		while(IFS1bits.AD1IF	==	0);		// wait until conversion is done
		IFS1bits.AD1IF		=	0;			// reset AD1IF
	}
	int *p = (int * )(&ADC1BUF0);
	int i = 0;
	unsigned int sum = 0;
	for(;i<numSamples;i++){
		sum = sum + p[i*4];
	}
	return sum/numSamples;
}

void configADC(unsigned char port){ // configura porto 14
	//desligar portas e configurar AN14
	TRISB = (TRISB & (0xFFFF ^ (0x01 <<port))) | (0x01 <<port); // set position " port " in TRISB to 1
	AD1PCFG = (AD1PCFG & (0xFFFF ^ (0x01 <<port)));	// set position " port " in AD1PCFGbits to 0
	AD1CON1bits.SSRC	= 7;
	AD1CON1bits.CLRASAM = 1;	// stops conversion;
	AD1CON3bits.SAMC = 16;
	AD1CON2bits.SMPI = numSamples-1;
	AD1CHSbits.CH0SA = port;
	AD1CON1bits.ON = 1;
	
	if(operationMode == 'I'){
		IPC6bits.AD1IP = 3;
		IEC1bits.AD1IE  = 1;
	}
}

void send2Segments(unsigned char value){
	static unsigned char disp = 0;
	static unsigned char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};

	selectDisplay(disp);
	LATB = ((LATB & 0xFF00) | (code[(value >> 4) & 0x0F] * disp + code[value & 0x0F] * !disp)); // (code[(value >> 4) & 0x0F] * disp + code[value & 0x0F] * !disp)
	disp = toggle(disp);
}

void selectDisplay(unsigned int disp){ // 0 => displ_low ; 1 displ_high
	LATBbits.LATB8 = !disp;
	LATBbits.LATB9 = disp;
}

char toggle(int value){
	return  ! value;
}

void configSegments(){ // configurar segmentos e selecção dos segmentos
	TRISB = TRISB & 0xFC00;
}

unsigned char toBCD(unsigned char value){
	return ((value / 10) << 4) + (value % 10);	
}

