#include "pic32.h"
#include "detpic32.h"


int main()
{
	configureUART1(115200,'N',1);
	configUART1Interrupt(); // configuração do interrupt da UART
	
	EnableInterrupts();
	while(1);
	
	return 0;
}

void _int_(24) isr_uart1(void){
	putc(getC()); // recebe e faz eco;
	IFS0bits.U1RXIF = 0;
	IFS0bits.U1RXIF = 0;

}

