#include "pic32.h"
#include "detpic32.h"


int main()
{
	configureUART1(115200,'N',1);
	
	TRISEbits.TRISE0 = 0; // Config RE0 as output
	
	while(1){
		while(!U1STAbits.TRMT); // wait until TRMT == 1 ( ou seja enquanto TX FIFO não estiver vazia faz polling );
		LATEbits.LATE1 = 1;
		putS("12345");
		LATEbits.LATE1 = 0;

		delay(1000);
	}
	return 0;
}
