#ifndef PIC32_H
#define PIC32_H


#define CALIBRATION_VALUE	5000	

// guião 4
void delay(unsigned int n_intervals);
void configureSegments();
void selectDisplay();
unsigned int readInputs();
void send2displays(unsigned int value);
char toggle(int flag);
unsigned char toBcd(unsigned char value);
void send2displaysPoint(unsigned int value);
void send2displaysBCD(unsigned int value);

// guiao 5

void init_RB14_as_AN14();
void configAD();
int readVoltage(int val);
void configAnalogInterrupts();



#endif
