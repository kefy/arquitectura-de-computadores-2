#include "pic32.h"
#include "detpic32.h"

int main()
{
	IFS1bits.AD1IF	=	0;	// RESET FLAG.
	init_RB14_as_AN14();	
	configAD(1,14); //  1 sample and AN14.
	configAnalogInterrupts(); // configura a interrupção para autorizar e atribui prioridade a leitura das entradas analógicas
	AD1CON1bits.ASAM =	1;	// inicia a conversão
	EnableInterrupts();
	while(1);

	
	return 0;
}

void _int_(27) isr_adc(void){
	
	printStr("Value: ");
	printInt(ADC1BUF0,16);
	AD1CON1bits.ASAM =	1;	// start conversion
	IFS1bits.AD1IF	=	0;	// Reset Flag.
}



