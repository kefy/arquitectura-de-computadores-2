#include "pic32.h"
#include "detpic32.h"

#define		displayFreq		100
#define		numSamples		8

volatile unsigned char value2display = 0; // global variable


int main()
{
	configureSegments();
	init_RB14_as_AN14();	
	configAD(numSamples,14); //  8 sample and AN14.
	configAnalogInterrupts(); // configura a interrupção para autorizar e atribui prioridade a leitura das entradas analógicas
	
	IFS1bits.AD1IF	=	0;	// RESET FLAG.
	EnableInterrupts();
	int i = 0;
	
	while(1){
		while(readCoreTimer() <= displayFreq);
		resetCoreTimer();
		
		if(i++ == 25){
			AD1CON1bits.ASAM =	1;	// inicia a conversão
			i = 0;
		}
		
		send2displaysBCD(value2display);
		printStr("\nVal: ");
		printInt10(value2display);
	}
	
	return 0;
}

void _int_(27) isr_adc(void)
{

	int pointerPos;
	int sum = 0;
	int *p = (int *)(&ADC1BUF0);
	for(pointerPos = 0; pointerPos< numSamples; pointerPos++)
		sum = sum + p[pointerPos * 4];
	
	
	int average = sum / numSamples;
	
	
	value2display = readVoltage(average);
	AD1CON1bits.ASAM =	1;	// inicia a conversão

	IFS1bits.AD1IF	=	0;	// Reset Flag.
}

