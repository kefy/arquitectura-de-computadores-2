#include "UART_DETPIC32.h"
#include <detpic32.h>

volatile circularBuffer txb;		// Transmission Buffer
volatile circularBuffer rxb;		// Reception Buffer


void _int_(24) isr_UART(void){

	if(IFS0bits.U1TXIF){
		if(txb.count>=0){
			U1TXREG = txb.head;
			txb.head++;
			txb.count--;
		
			if(txb.count == 0){
				DisableUart1RxInterrupt();
				IFS0bits.U1TXIF = 0; // baixar a flag
			}
		}
	}	
}

void comDrv_flushRx(void){
	rxb.count	=	0;
	rxb.head	=	0;
	rxb.tail	=	0;
}

void comDrv_flushTx(void){
	txb.count	=	0;
	txb.head	=	0;
	txb.tail	=	0;
}

void comDrv_putc(char ch){
	while(txb.count == BUF_SIZE);			//Wait while buffer is full
	txb.data[txb.tail] = ch;
	txb.tail = (txb.tail + 1) & INDEX_MASK;	// increment "tail"
	DisableUart1RxInterrupt();				// Begin of critical section
	txb.count++;							// Increment TX count
	EnableUart1TxInterrup();				// End of critical section
}

void comDrv_puts(char *s){
	
	do{
		comDrv_putc(*s);
		s++;
	}while(*s != '\0');
	
}
	
void comDrv_config(int baudRate, char parity, int stopbits){
	
	unsigned int parityNumber = 0;
	
	if (baudRate < 600 || baudRate >115200){
		baudRate = 115200;
	}
	
	if (stopbits != 1 || stopbits!=2)
		stopbits = 1;
	
	switch(parity){
		case 'N':
			parityNumber = 00;
		break;
		
		case 'E':
			parityNumber = 01;
		break;
		
		case 'O':
			parityNumber = 10;
		break;
	}
		
	
	U1BRG = (PBCLK + 8 * baudRate)/(16*baudRate) - 1;
	U1MODEbits.BRGH = 0; // Standard Speed mode
	U1MODEbits.PDSEL = parityNumber; // 8 bits no parity
	U1MODEbits.STSEL = stopbits - 1; // 1 stop Bit
	U1STAbits.UTXEN = 1; // inicia TX
	U1STAbits.URXEN = 1; // inicia RX
	U1STAbits.URXISEL = 0;
	U1STAbits.UTXISEL = 0;
	U1MODEbits.ON = 1;
	
	
}
	
	
