#ifndef UART_DETPIC32_H
#define UART_DETPIC32_H

#define BUF_SIZE	32
#define INDEX_MASK	(BUF_SIZE - 1)

#define DisableUart1RxInterrupt()	IEC0bits.U1RXIE = 0
#define EnableUart1RxInterrupt()	IEC0bits.U1RXIE = 1
#define DisableUart1TxInterrup()	IEC0bits.U1TXIE = 0
#define EnableUart1TxInterrup()		IEC0bits.U1TXIE = 1


//Estrutura de dados
typedef struct
{
	unsigned char data[BUF_SIZE];
	unsigned int head;
	unsigned int tail;
	unsigned int count;
}circularBuffer;


//Declaração de funções
void _int_24isr_UART(void);
void comDrv_flushRx(void);
void comDrv_flushTx(void);
void comDrv_putc(char ch);
void comDrv_puts(char *s);
void comDrv_config(int baudRate,char parity, int );





#endif
