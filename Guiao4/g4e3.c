#include "detpic32.h"
#include "pic32.h"

int main()
{
	static const unsigned char code[] = {0b00000010,0b00000001,0b01000000,0b00100000,0b00010000,0b00000100,0b00001000};			
	configureSegments();

	while(1){
		printStr("Introduza uma letra: ");
		char a = getChar();
		selectDisplay(0); // selecciona display menos significativo
		LATB  = (LATB & 0xFF00) | code[a-97] ;
	}
	
	return 0;
	
}

