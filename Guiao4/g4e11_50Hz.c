#include "detpic32.h"
#include "pic32.h"

#define		displayRefreshTime		20 // freq de 50 hz
#define		numTimes				10 // 10 * 20 = 200 ms  = 5 Hz

int main()
{
	configureSegments();	

	int counter = 0;
	int i;
	
	while(1){
		 i = 0;
		 do{
			  delay(displayRefreshTime);
			  send2displays(counter); // refrescamento displays 100hz
		  }while(++i < numTimes);
		  counter++;
		 
		  if(counter == 256)
				counter = 0;
		  
	}
	
	return 0;
	
}
