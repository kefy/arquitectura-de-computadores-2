#include "detpic32.h"
#include "pic32.h"

#define		displayRefreshTime		10 // freq de 100 hz
#define		numTimes				20 // 10 * 10 = 200 ms  = 5 Hz

int main()
{
	configureSegments();	

	int counter = 0;
	int i;
	int c;
	
	while(1){
		 i = 0;

		 do{
			 delay(displayRefreshTime);
			  send2displaysPoint(toBcd(counter)); // refrescamento displays 100hz função com imprimir ponto em par ou impar incluido
		  }while(++i < numTimes);
		  counter++;
		  
		  if(counter == 60){
				counter = 0; 
				c = 0;
			do{
				// 500 ms 0
				i = 0;
				do{
					send2displaysPoint(counter); // refrescamento displays 100hz função com imprimir ponto em par ou impar incluido
					delay(displayRefreshTime);
				}while(++i < 50);
				
				
				// 500 ms apagado
				LATBbits.LATB8 = 0;
				LATBbits.LATB9 = 0;
				delay(500);
		
			
			}while(++c <= 5);
		  } 
	}
	
	return 0;
	
}


