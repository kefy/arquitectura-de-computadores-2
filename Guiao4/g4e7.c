#include "detpic32.h"
#include "pic32.h"

int main()
{
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
	configureSegments();

	while(1){
		
		int number = readInputs(PORTE, 0x00F0, 4);
		printInt(PORTE,2);
		selectDisplay(0); // selecciona display menos significativo, para o mais colocar argumento a 1
		LATB  = (LATB & 0xFF00) | code[number] ;
		delay(1000);
	}
	return 0;
	
}



