#include "detpic32.h"
#include "pic32.h"

#define		displayRefreshTime		50
#define		numTimes				4 // 4 * 5 = 20 Hz

int main()
{
	configureSegments();	

	int counter = 0;
	int i;
	
	while(1){
		 i = 0;
		 do{
			  delay(displayRefreshTime);
			  send2displays(counter); // refrescamento displays 100hz
		  }while(++i < numTimes);
		  counter++;
		 
		  if(counter == 256)
				counter = 0;
		  
	}
	
	return 0;
	
}

