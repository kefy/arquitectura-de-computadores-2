#include "pic32.h"
#include "detpic32.h"



void delay(unsigned int n_intervals){
	volatile unsigned int i;
	
	for(; n_intervals != 0 ; n_intervals--){
		for(i = CALIBRATION_VALUE; i != 0 ; i--);
	}
}

void configureSegments(){
	TRISB = (TRISB & 0XFC00); // configure OUTPUTS
}

void selectDisplay(unsigned int disp){
	LATBbits.LATB8 = !disp;
	LATBbits.LATB9 = disp;
	
}

unsigned int readInputs(unsigned int port, unsigned int mask, unsigned int shift){	
	return (port & mask) >> shift ;
}

void send2displays(unsigned int value){
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
       
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);

}

char toggle(int flag){
	return !flag;
}

unsigned char toBcd(unsigned char value){
	return ((value / 10) << 4) + (value % 10);
	
}

void send2displaysPoint(unsigned int value){
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
       
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);	    
   	LATB = LATB | (0x0080 * ((0 == value % 2) * displayFlag + (0 != value % 2) * !displayFlag));

	
}






