#ifndef PIC32_H
#define PIC32_H


#define CALIBRATION_VALUE	5000

void delay(unsigned int n_intervals);
void configureSegments();
void selectDisplay();
unsigned int readInputs();
void send2displays(unsigned int value);
char toggle(int flag);
unsigned char toBcd(unsigned char value);
void send2displaysPoint(unsigned int value);



#endif
