#include "pic32.h"
#include "detpic32.h"

int main()
{
	initTimer3();
	configTimer3Interrupt();
	EnableInterrupts();
	
	while(1);
	return 0;
}

void _int_(12) isr_T3(void){
	static char state = 0; 
	if(state)
		putChar('.');
	IFS0bits.T3IF = 0;	
	state = toggle(state);
}

