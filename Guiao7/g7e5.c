#include "pic32.h"
#include "detpic32.h"

volatile unsigned char value2display = 0; // Global variable
void configureAll();
void reset();

int main()
{

	configureAll();
	TRISE = TRISE | 0x0030;
	reset();
	EnableInterrupts();	
	int activated = 1;
	char RE5;
	char RE4;
	while(1){	
		RE5 = PORTEbits.RE5;
		RE4 = PORTEbits.RE4;
		activated = !RE5 && RE4;
		if(activated){
			activated = 0;
		}
		else{
			activated = 1;
		}
		
		IEC0bits.T1IE = activated;

		

		
	}
	return 0;

}


#define numSamples	8

void _int_(27) isr_adc(void)
{

	int pointerPos;
	int sum = 0;
	int *p = (int *)(&ADC1BUF0);
	for(pointerPos = 0; pointerPos< numSamples; pointerPos++)
		sum = sum + p[pointerPos * 4];
	
	int average = sum / numSamples;
	value2display = toBcd(readVoltage(average));
	AD1CON1bits.ASAM =	1;	// inicia a conversão
	IFS1bits.AD1IF	=	0;	// Reset Flag.
}

void _int_(4) isr_T1(void){ // T1 interrupt
	AD1CON1bits.ASAM =	1;	// Start A/D conversion
	IFS0bits.T1IF = 0;		// reset T1IF flag
}

void _int_(12) isr_T3(void){ // T3 interrupt
	send2displays(value2display);
	IFS0bits.T3IF = 0;	
}

void configureAll(){
	
	// config segments
	configureSegments();
	
	// Config AN
	init_RB14_as_AN14();
	configAD(8, 14);
	configAnalogInterrupts();
	
	// Config Timers
	initTimer1();
	initTimer3();
	configTimer1Interrupt();
	configTimer3Interrupt();
}

void reset(){
	IFS0bits.T3IF = 0;	// reset T3 flag
	IFS0bits.T1IF = 0;	// reset T1 flag
	IFS1bits.AD1IF = 0; 	// reset AD flag
}





