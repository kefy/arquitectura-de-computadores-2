#include "detpic32.h"

int main()
{
	TRISE = (TRISE & 0xFFF0) | 0x00C0; // RE0 as output and RE6 as input
	
	while(1){
		int a = PORTEbits.RE6;
		int b = PORTEbits.RE7;	
		LATEbits.LATE0 = a & b;
		LATEbits.LATE1 = a | b;
		LATEbits.LATE2 = a ^ b;
		LATEbits.LATE3 = (!(a || b)) & 0x0001;
	}
	
	return 0;
}




