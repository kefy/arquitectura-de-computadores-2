#include "detpic32.h"

void binaryCounter();
void jhonsonCounter();
unsigned int counter();
void readInputs();


int main()
{
	TRISE = (TRISE & 0xFFF0) | 0x00C0; 
	int state = 0;
	int re6 = 0;
	int re7 = 0;
	int lastRE6 = 0;
	int lastRE7 = 0;
	
		
	while(1){
		readInputs(&re6, &re7);
		int tmp = counter();
		printInt10(state);
		
		if(tmp){
			if(re7){
				if(state == 0)
					state = 15;
				else
					state--;
			}
			else{
				if(state == 15)
					state = 0;
				else
					state++;
			}
		}
		
		if(re6){
			jhonsonCounter(state);
		}
		else
			binaryCounter(state);
			
		state = state * !(lastRE6 ^ re6);
		state = state * !(lastRE7 ^ re7);

		lastRE6 = re6;
		lastRE7 = re7;
		
	}
	
	
	return 0;
}

void binaryCounter(unsigned int state){
	LATE = (LATE & 0xFFF0) | (state) ;
}

void jhonsonCounter(unsigned int state){
	 const char array[] = {0b0000,0b0001,0b0011,0b0111,0b1111,0b1110,0b1100,0b1000};
	 if(state>=8)
		LATE = (LATE & 0xFFF0) | array[state-8];
     else
		LATE = (LATE & 0xFFF0) | array[state];
	
}

unsigned int counter(){
	int tmp;
	tmp = 0;
	int core = readCoreTimer();
	if( core >= (FREQ/4)){
		tmp = 1;
		resetCoreTimer();
	}
	
	return tmp;
}

void readInputs(int *a, int *b ){
	*a = PORTEbits.RE6;
	*b = PORTEbits.RE7;
}









