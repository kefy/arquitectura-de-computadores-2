#include "detpic32.h"

int main()
{
	TRISE = (TRISE & 0xFFFE) | 0x0040; // RE0 as output and RE6 as input
	while(1){
		LATEbits.LATE0 = !PORTEbits.RE6;
	}
	
	return 0;
}

