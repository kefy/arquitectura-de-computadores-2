#include "pic32.h"
#include "detpic32.h"



void delay(unsigned int n_intervals){
	volatile unsigned int i;
	
	for(; n_intervals != 0 ; n_intervals--){
		for(i = CALIBRATION_VALUE; i != 0 ; i--);
	}
}
void configureSegments(){
	TRISB = (TRISB & 0XFC00); // configure OUTPUTS
}

void selectDisplay(unsigned int disp){
	LATBbits.LATB8 = !disp;
	LATBbits.LATB9 = disp;
}

unsigned int readInputs(unsigned int port, unsigned int mask, unsigned int shift){	
	return (port & mask) >> shift ;
}

void send2displays(unsigned int value){ // apresenta valores nos displays em HexaDecimal
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
       
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);
}

void send2displaysBCD(unsigned int value){ // apresenta valores nos displays em HexaDecimal
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
    value = toBcd(value);
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);
}

char toggle(int flag){
	return !flag;
}

unsigned char toBcd(unsigned char value){
	return ((value / 10) << 4) + (value % 10);
	
}

void send2displaysPoint(unsigned int value){
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
       
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);	    
   	LATB = LATB | (0x0080 * ((0 == value % 2) * displayFlag + (0 != value % 2) * !displayFlag));
}


void init_RB14_as_AN14(){
	TRISBbits.TRISB4	=	1;
	AD1PCFGbits.PCFG4	=	0;
}

void configAD(int numSamples, int analogChannel){
	AD1CON1bits.SSRC	=	7;					// Conversion trigger selection bits: in this mode an internal counter ends sampling and starts conversion.
	AD1CON1bits.CLRASAM	=	1;					// Stop conversions when the 1st A/D converter interrut is generated. At the same time, hardware clears the ASAM bit
	AD1CON3bits.SAMC	=	16;					// Sample time is 16 TAD (TAD = 100ns). ( time it takes to colect samples
	AD1CON2bits.SMPI	=	numSamples - 1;		// interrupt is generated after numSamples. numSamples is the number of consecutive samples
	AD1CHSbits.CH0SA	=	analogChannel;		// select analog channel - 0 to 15
	AD1CON1bits.ON		=	1;					// enable a/d converter
}

int readVoltage(int val){
	return ((val * 33 + 511)/1023);
}






