#include "pic32.h"
#include "detpic32.h"

int main()
{
	// configure A/D module and RB14 as analog input
	// Pinmode
	init_RB14_as_AN14();
	configAD(1,14); //  1 sample and AN14.
	
	while(1){
		
		AD1CON1bits.ASAM	=	1;			// starts conversion
		while(IFS1bits.AD1IF	==	0);		// wait until conversion is done
		printStr("\nValue: ");
		printInt(ADC1BUF0,16);
		IFS1bits.AD1IF		=	0;			// reset AD1IF
	}
	
	return 0;
}

