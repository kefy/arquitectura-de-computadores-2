#include "pic32.h"
#include "detpic32.h"

#define		numSamples		4
#define		displayFreq		100 	// colocar a 200 caso queira ver mais nitidamente
#define		anSampling		25		// caso Freq = 200 colocar 50.

int main()
{
	// configure A/D module and RB14 as analog input
	// Pinmode
	init_RB14_as_AN14();
	configAD(numSamples,14); //  1 sample and AN14.
	configureSegments();	// start segments
	
	int i = 0;
	int v = 0;
	while(1){
		while(readCoreTimer() <= displayFreq); // wait 10 ms using core timer; 
		resetCoreTimer();
		if(i++ == anSampling){				// 250 ms - porque sao 4 sequencias de conversao de 4 amostrsa por segundo. 
			AD1CON1bits.ASAM	=	1;			// starts conversion
			while(IFS1bits.AD1IF	==	0);		// wait until conversion is done
			int *p = (int *)(&ADC1BUF0);
			
		//********calcular média*****************
			int sum = 0;
			for(i = 0; i< numSamples; i++){
				sum = sum + p[i*4];
			}
			int average = sum/numSamples;
		//***************************************
				
			v = readVoltage(average);
			IFS1bits.AD1IF		=	0;			// reset AD1IF
			i = 0;
		}
		send2displaysBCD(v);
	}
	return 0;
}

