#include "pic32.h"
#include "detpic32.h"

#define		numSamples	4

int main()
{
	// configure A/D module and RB14 as analog input
	// Pinmode
	init_RB14_as_AN14();
	configAD(numSamples,14); //  1 sample and AN14.
	while(1){
		AD1CON1bits.ASAM	=	1;			// starts conversion
		while(IFS1bits.AD1IF	==	0);		// wait until conversion is done
		
		int *p = (int *)(&ADC1BUF0);
		int i = 0;
		
		//********calcular média*****************
		int sum = 0;
		for(i = 0; i< numSamples; i++){
			sum = sum + p[i*4];
		}
		int average = sum/numSamples;
		//***************************************
		
		int v = readVoltage(average);
		printStr("\nTensão: ");
		printInt10(v);
		
		
		IFS1bits.AD1IF		=	0;			// reset AD1IF
	}
	return 0;
}

