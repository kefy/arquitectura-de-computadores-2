#include "pic32.h"

int main()
{
	configureTimer3(7,39062,'E');	// 7 = prescaler = 256 	, Interrupts enable
	EnableInterrupts();
	while(1);
	return 0;
}

void _int_(12) isr_T3(void){
	putChar('.');
	IFS0bits.T3IF = 0;
}


