#include "pic32.h"

#define 	numSamples	8

void configureAll();
void reset();

volatile unsigned char 	value2display;

int main(){
	
	const static unsigned char pwnValues[] = {3, 15, 40, 90};
	
	//TRISDbits.TRISD1 = 0; 					//RD1 => output
	configureAll();
	reset();
	EnableInterrupts();
	while(1){
		int menuSelection = (PORTE >> 4) & 0x03;
		int brigthnessSelection = (PORTE >> 6) & 0x0003;
		
		printStr("\nhere ");
		printInt(menuSelection,10);
		switch(menuSelection){
			case 0:					//measure input voltage
				IEC0bits.T1IE = 1;
				setPWM(0);
				break;
			case 1:	//FREEZE
				IEC0bits.T1IE = 0;	// disable T1 interrups
				setPWM(100);
				break;
			case 2:
				IEC0bits.T1IE = 0;	// disable T1 interrupts
				setPWM(pwnValues[brigthnessSelection]);
				value2display = toBcd(pwnValues[brigthnessSelection]);
				break;
			default:
				break;
				
		}
	}
	return 0;
}

void _int_(4) isr_T1(void){ // f = 4 hz   - AD CONVERSION
	AD1CON1bits.ASAM =	1;	// start A/D conversion
	IFS0bits.T1IF = 0;		// Reset timer 1 flag
}

void _int_(12) isr_T3(void){// f = 100 hz - send to displays
	send2displays(value2display);
	IFS0bits.T3IF = 0;		// reset timer 3 flag
}

void _int_(27) isr_adc(void){
	int pointerPos;
	unsigned int sum = 0;
	int *p = (int *)(&ADC1BUF0);
	for(pointerPos = 0; pointerPos< numSamples; pointerPos++)
		sum = sum + p[pointerPos * 4];
	int average = sum / numSamples;
	value2display = toBcd(readVoltage(average));
	
	IFS1bits.AD1IF = 0;		// reset AD1IF flag	
}

void configureAll(){
	configureSegments();			// configuração dos 7 segmentos
	
	init_RB14_as_AN14();			// desliga RB14 e liga AN14
	configAD(8,14);					// configura a AD 8 samples AN14
	configAnalogInterrupts();		// activa interrutps em ADC
	
	configureTimer1(3,19531,'E');	// 3 = prescaler => 256 , Interrupts enable:  4 	HZ
	configurePWM_Timer3(100,2,49999,0b110
	); // 110 = PWM mode on OCx; Fault pin disabled
}

void reset(){
	IFS1bits.AD1IF	= 0;
	IFS0bits.T1IF 	= 0;
	IFS0bits.T3IF	= 0;
}




