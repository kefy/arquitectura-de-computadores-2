#include "pic32.h"

int main()
{
	configureTimer3(7,39062,'E');	// 7 = prescaler = 256 	, Interrupts enable
	EnableInterrupts();
	while(1);
	return 0;
}


void _int_(12) isr_T3(void){ // f = 1 hz = 1 seg
	static char val = 0;
	if(val)
		putChar('.');
	IFS0bits.T3IF = 0;
	val = toggle(val);
}


