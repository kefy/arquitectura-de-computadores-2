#include "pic32.h"

#define 	numSamples	8

void configureAll();
void reset();

volatile unsigned char 	value2display;

int main(){
	configureAll();
	reset();
	EnableInterrupts();
	while(1){
		char RE5 = PORTEbits.RE5;
		char RE4 = PORTEbits.RE4;
		printInt10(value2display);
		if((RE5 == 0) && (RE4 == 1))
				IEC0bits.T1IE = 0;	// Enable timer T3 interrupts
		else
				IEC0bits.T1IE = 1;

			
		
		
		
	}
	return 0;
}

void _int_(4) isr_T1(void){ // f = 4 hz   - AD CONVERSION
	AD1CON1bits.ASAM =	1;	// start A/D conversion
	IFS0bits.T1IF = 0;		// Reset timer 1 flag
}

void _int_(12) isr_T3(void){// f = 100 hz - send to displays
	send2displays(value2display);
	IFS0bits.T3IF = 0;		// reset timer 3 flag
}

void _int_(27) isr_adc(void){
	int pointerPos;
	unsigned int sum = 0;
	int *p = (int *)(&ADC1BUF0);
	for(pointerPos = 0; pointerPos< numSamples; pointerPos++)
		sum = sum + p[pointerPos * 4];
	int average = sum / numSamples;
	printStr("\nValue");
	printInt(average,16);
	value2display = toBcd(readVoltage(average));
	
	IFS1bits.AD1IF = 0;		// reset AD1IF flag	
}

void configureAll(){
	configureSegments();			// configuração dos 7 segmentos
	
	init_RB14_as_AN14();			// desliga RB14 e liga AN14
	configAD(8,14);					// configura a AD 8 samples AN14
	configAnalogInterrupts();		// activa interrutps em ADC
	
	configureTimer1(3,19531,'E');	// 3 = prescaler => 256 , Interrupts enable:  4 	HZ
	configureTimer3(2,49999,'E');	// 2 = prescaler => 4 	, Interrupts enable:  100	HZ
}

void reset(){
	IFS1bits.AD1IF	= 0;
	IFS0bits.T1IF 	= 0;
	IFS0bits.T3IF	= 0;
}


