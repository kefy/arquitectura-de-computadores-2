#ifndef PIC32_H
#define PIC32_H
#include "detpic32.h"

#define CALIBRATION_VALUE	5000	

// guião 4
void delay(unsigned int n_intervals);
void configureSegments();
void selectDisplay();
unsigned int readInputs();
void send2displays(unsigned char value);
char toggle(int flag);
unsigned char toBcd(unsigned char value);
void send2displaysPoint(unsigned int value);
void send2displaysBCD(unsigned int value);
void configureTimer3(int tckps, unsigned int pr3, char interrupt);
void configureTimer1(int tckps, unsigned int pr1, char interrupt);
void configurePWM_Timer3(unsigned int dutyCycle, int tckps, unsigned int pr3, char ocm);
void setPWM(unsigned int dutyCycle);

// guiao 5

void init_RB14_as_AN14();
void configAD();
unsigned int readVoltage(unsigned int val);
void configAnalogInterrupts();



// guiao 8

void configureUART1(unsigned int baudRate, char parity, char stopBit);
void putc(char byte2display);
void putS(char *str);
char getC(void);
void configUART1Interrupt();

#endif
