#include "pic32.h"

#define 	numSamples	8

void configureAll();
void reset();

volatile unsigned char 	value2display;

int main(){
	configurePWM_Timer3(12500,2,49999,6); // 110 = PWM mode on OCx; Fault pin disabled
	EnableInterrupts();
	while(1);
	return 0;
}

void _int_(12) isr_T3(void){// f = 100 hz - send to displays
	IFS0bits.T3IF = 0;		// reset timer 3 flag
}





