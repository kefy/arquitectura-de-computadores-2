#include "pic32.h"



void delay(unsigned int n_intervals){
	volatile unsigned int i;
	
	for(; n_intervals != 0 ; n_intervals--){
		for(i = CALIBRATION_VALUE; i != 0 ; i--);
	}
}

void configureSegments(){
	TRISB = (TRISB & 0XFC00); // configure OUTPUTS
}

void selectDisplay(unsigned int disp){
	LATBbits.LATB8 = !disp;
	LATBbits.LATB9 = disp;
}

unsigned int readInputs(unsigned int port, unsigned int mask, unsigned int shift){	
	return (port & mask) >> shift ;
}

void send2displays(unsigned char value){ // apresenta valores nos displays em HexaDecimal
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);
    delay(100);
}

void send2displaysBCD(unsigned int value){ // apresenta valores nos displays em HexaDecimal
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
    static unsigned char displayFlag = 0;
    value = toBcd(value);
	selectDisplay(displayFlag);
	LATB  = (LATB & 0xFF00) | (code[0x000F & value] * !displayFlag + code[(value>>4) & 0x000F] * displayFlag); // evita-se colocar ifs
    displayFlag = toggle(displayFlag);
    
}

char toggle(int flag){
	return !flag;
}

unsigned char toBcd(unsigned char value){
	return ((value / 10) << 4) + (value % 10);
	
}

void init_RB14_as_AN14(){
	TRISBbits.TRISB14	=	1;
	AD1PCFGbits.PCFG4	=	0;
}

void configAD(int numSamples, int analogChannel){
	AD1CON1bits.SSRC	=	7;					// Conversion trigger selection bits: in this mode an internal counter ends sampling and starts conversion.
	AD1CON1bits.CLRASAM	=	1;					// Stop conversions when the 1st A/D converter interrut is generated. At the same time, hardware clears the ASAM bit
	AD1CON3bits.SAMC	=	16;					// Sample time is 16 TAD (TAD = 100ns). ( time it takes to colect samples
	AD1CON2bits.SMPI	=	numSamples - 1;		// interrupt is generated after numSamples. numSamples is the number of consecutive samples
	AD1CHSbits.CH0SA	=	analogChannel;		// select analog channel - 0 to 15
	AD1CON1bits.ON		=	1;					// enable a/d converter
}

unsigned int readVoltage(unsigned int val){
	return ((val * 33 + 511)/1023);
}

void configAnalogInterrupts(){
	IPC6bits.AD1IP = 3; // prioridade = 3
	IEC1bits.AD1IE = 1;	// autorizar interrupts
}

void configureTimer1(int tckps, unsigned int pr1, char interrupt){ 
	IFS0bits.T1IF = 0;
	T1CONbits.TCKPS = tckps; // type B
	PR1 = pr1;
	TMR1 = 0;			// reset timer T1 count register.
	T1CONbits.TON = 1;	// Enable timer T1( must be the last command of the timer configuaration)

	if(interrupt == 'E'){
		IFS0bits.T1IF = 0;	// reset timer T1 interrupt flag
		IPC1bits.T1IP = 2;	// Interrupt priority [1..6]
		IEC0bits.T1IE = 1;	// Enable timer T1 interrupts
	}
}

void configureTimer3(int tckps, unsigned int pr3, char interrupt){
	IFS0bits.T3IF = 0;
	T3CONbits.TCKPS = tckps;	
	PR3 = pr3;
	TMR3 = 0;			// reset timer T3 count register.
	T3CONbits.TON = 1;	// Enable timer T3 ( must be the last command of the timer configuaration)
	
	if(interrupt == 'E'){
		IFS0bits.T3IF = 0;	// reset timer T3 interrupt flag
		IPC3bits.T3IP = 5;	// Interrupt priority [1..6]
		IEC0bits.T3IE = 1;	// Enable timer T3 interrupts
	}
	
}

void configurePWM_Timer3(unsigned int dutyCycle,int tckps, unsigned int pr3, char ocm){
	configureTimer3(tckps,pr3,'E');
	OC1CONbits.OCM 		= ocm;
	OC1CONbits.OCTSEL	= 1;
	OC1RS				= ((PR3 + 1) * dutyCycle)/100;	
	OC1CONbits.ON		= 1;		// Enable OC1 module
}

void setPWM(unsigned int dutyCycle){
	OC1RS = ((PR3 + 1) * dutyCycle)/100;
}



void configureUART1(unsigned int baudRate, char parity, char stopBit){
	
	if(baudRate < 600 || baudRate > 115200)
		baudRate = 115200;
	
	U1BRG = (PBCLK + 8 * baudRate)/(16*baudRate) - 1; // configure baudRate
	
	switch (parity){
		case 'N':
			U1MODEbits.PDSEL = 0b00; // not even parity
			break;
		case 'E':
			U1MODEbits.PDSEL = 0b01; // even parity
			break;
		case 'O':
			U1MODEbits.PDSEL = 0b10; // odd parity
			break;
		default:
			U1MODEbits.PDSEL = 0b00;
			break;
	}
	
	if(stopBit != 1 || stopBit != 2)
		stopBit = 1;
	
	U1MODEbits.STSEL = stopBit; 	// stopBits 
	
	U1STAbits.URXEN = 1; 			// receiver Enable Bit
	U1STAbits.UTXEN = 1;			// transmitter Enable Bit	
	U1MODEbits.ON	= 1;
}

void putc(char byte2send){
	while(U1STAbits.UTXBF == 1);
	U1TXREG = byte2send;
}

void putS(char *str){
	char *p = str;
	while(*p != '\0'){
		putc(*p);
		p++;
	}
}

char getC(void){
	if(U1STAbits.OERR == 1)
		U1STAbits.OERR = 0;
	while(U1STAbits.URXDA == 0);
	if(U1STAbits.FERR || U1STAbits.PERR){
		return 0;
	}
	return U1RXREG;
}

void configUART1Interrupt(){
	IPC6bits.U1IP = 5; // definir prioridade. (default = 0 => não funciona)
	IEC0bits.U1RXIE = 1; // Enable Receção
	IEC0bits.U1TXIE = 1; // Enable Transmição

}







