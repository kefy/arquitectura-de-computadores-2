#include "pic32.h"

int main()
{
	configureTimer3(7,39062,'D');	// 7 = prescaler = 256 
	while(1){
		while(IFS0bits.T3IF == 0);		// until flag == 1
		IFS0bits.T3IF = 0; 			// reset flag
		putChar('.');
	}
	return 0;
}

