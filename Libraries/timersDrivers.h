#ifndef	timersDrivers_h
#define timersDrivers

#include "detpic32.h"
#define timer1OperationMode		'I'
#define timer2OperationMode		'I'
#define timer3OperationMode		'I'
#define timer4OperationMode		'I'
#define timer5OperationMode		'I'

#define resetT1()				IFS0bits.T1IF = 0;
#define resetT2()				IFS0bits.T2IF = 0;
#define resetT3()				IFS0bits.T3IF = 0;
#define resetT4()				IFS0bits.T4IF = 0;
#define resetT5()				IFS0bits.T5IF = 0;


void configTimer1(unsigned int prescaler, unsigned char tckps);
void configTimer2(unsigned int prescaler, unsigned char tckps);
void configTimer3(unsigned int prescaler, unsigned char tckps);
void configTimer4(unsigned int prescaler, unsigned char tckps);
void configTimer5(unsigned int prescaler, unsigned char tckps);


// temporary functins. I hope i will understand them better and improve them
void configPWM(unsigned int dutyCycle, unsigned int prescaler, unsigned char tckps);
void setPWM(unsigned int dutyCycle);

#endif
