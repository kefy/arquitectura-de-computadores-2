#include "analogDriver.h"

void configADC(unsigned int port){
	
	TRISB = (TRISB & (0xFFFF ^ (0x01 <<port))) | (0x01 <<port);
	AD1PCFG = (AD1PCFG & (0xFFFF ^ (0x01 <<port))); 
	
	AD1CON1bits.SSRC = 7;
	AD1CON1bits.CLRASAM = 1;
	AD1CON3bits.SAMC = 16;
	AD1CON2bits.SMPI = numSamples-1;
	AD1CHSbits.CH0SA = port;
	AD1CON1bits.ON = 1;
	
#if	adcOperationMode == 'I'
		IFS1bits.AD1IF = 0;
		IPC6bits.AD1IP = 3;
		IEC1bits.AD1IE = 1;

#endif		
}

int analogRead(unsigned int port){

#if adcOperationMode == 'P'
		configADC(14);
		AD1CON1bits.ASAM = 1;
		while(IFS1bits.AD1IF == 0);
#endif
	
	int *p = (int *)(&ADC1BUF0);
	int sum = 0;
	int i;
	for(i = 0; i < numSamples; i++){
		sum = sum + p[i*4];
	}
	return sum/numSamples;
}

char readVoltage(char value){
	return (analogRead(14)*33)/1023;
}



	
	




