#ifndef segmentsDriver_h
#define segmentsDriver_h

#include "detpic32.h"

void configSegments();
void segments2Displays(unsigned char value);
void selectDisplay(unsigned int value);
unsigned char toggle(unsigned char value);



#endif
