#include "segmentsDriver.h"

void configSegments(){
	TRISB = (TRISB & 0xFC00) ;
}

void segments2Displays(unsigned char value){
	static char code[] = {0x77, 0x41, 0x3b, 0x6b, 0x4d, 0x6e, 0x7e, 0x43, 0x7f, 0x6f, 0x5f, 0x7c,0x36, 0x79, 0x3e, 0x1e};
	static unsigned char displayFlag = 0;
	printInt10(value);
	selectDisplay(displayFlag);
	LATB = (LATB & 0xFF00) | (code[value & 0x0F]*!displayFlag + code[(value >> 4) & 0x0F]*displayFlag);
	displayFlag = toggle(displayFlag);
}

unsigned char toggle(unsigned char value){
	return !value;
}

void selectDisplay(unsigned int disp_number){
	LATBbits.LATB8 = !disp_number;
	LATBbits.LATB9 = disp_number;
}
