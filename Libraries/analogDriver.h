#ifndef analogDriver_h
#define analogDriver_h

#include "detpic32.h"

#define numSamples				8
#define adcOperationMode	   'I'	


void configADC(unsigned int port);
int analogRead(unsigned int port);
char readVoltage(char value);

#endif
