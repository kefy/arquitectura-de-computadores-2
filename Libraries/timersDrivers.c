#include "timersDrivers.h"

//all timers aren't tested

void configTimer1(unsigned int prescaler, unsigned char tckps){

	T1CONbits.TCKPS = tckps;
	PR1 = prescaler;
	TMR1 = 0;
	T1CONbits.TON = 1;
	
#if (timer1OperationMode == 'I') // save space in pic32 flash memory
	IFS0bits.T1IF = 0;
	IPC1bits.T1IP = 2;
	IEC0bits.T1IE = 1;
	
#endif
}

void configTimer2(unsigned int prescaler, unsigned char tckps){

	T2CONbits.TCKPS = tckps;
	PR2 = prescaler;
	TMR2 = 0;
	T2CONbits.TON = 1;
	
#if (timer2OperationMode == 'I') // save space in pic32 flash memory
	IFS0bits.T2IF = 0;
	IPC2bits.T2IP = 2;
	IEC0bits.T2IE = 1;
	
#endif
}

void configTimer3(unsigned int prescaler, unsigned char tckps){

	T3CONbits.TCKPS = tckps;
	PR3 = prescaler;
	TMR3 = 0;
	T3CONbits.TON = 1;
	
#if (timerOperationMode == 'I') // save space in pic32 flash memory
	IFS0bits.T3IF = 0;
	IPC1bits.T3IP = 2;
	IEC0bits.T3IE = 1;
	
#endif
}

void configTimer4(unsigned int prescaler, unsigned char tckps){

	T4CONbits.TCKPS = tckps;
	PR4 = prescaler;
	TMR4 = 0;
	T4CONbits.TON = 1;
	
#if (timerOperationMode == 'I') // save space in pic32 flash memory
	IFS0bits.T4IF = 0;
	IPC1bits.T4IP = 2;
	IEC0bits.T4IE = 1;
	
#endif
}

void configTimer5(unsigned int prescaler, unsigned char tckps){

	T5CONbits.TCKPS = tckps;
	PR5 = prescaler;
	TMR5 = 0;
	T1CONbits.TON = 1;
	
#if (timerOperationMode == 'I') // save space in pic32 flash memory
	IFS0bits.T5IF = 0;
	IPC1bits.T5IP = 2;
	IEC0bits.T5IE = 1;
	
#endif
}


// This PWN only uses OC1 and TIMER 2
void configPWM(unsigned int dutyCycle, unsigned int prescaler, unsigned char tckps){
	configTimer2(prescaler,tckps);
	OC1CONbits.OCM = 6;
	OC1CONbits.OCTSEL = 0;
	setPWM(dutyCycle);
	OC1CONbits.ON = 1;
}

void setPWM(unsigned int dutyCycle){
	OC1RS = ((PR2 + 1) * dutyCycle)/100;
}

