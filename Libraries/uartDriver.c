#include "uartDriver.h"

void configUart(unsigned int baudRate, char parity, char stopBits){
	
	U1BRG = ((PBCLK + 8 * baudRate)/(16*baudRate)) - 1 ;
	
	if(parity == 'N')
		U1MODEbits.PDSEL = 0;
	else if(parity  == 'E')
		U1MODEbits.PDSEL = 1;
	else if(parity == 'O')
		U1MODEbits.PDSEL = 2;
	else
		U1MODEbits.PDSEL = 0;
		
	if(stopBits != 1 && stopBits != 2)
		stopBits = 1;
	
	U1MODEbits.STSEL = stopBits - 1;
	U1STAbits.URXEN = 1;
	U1STAbits.UTXEN = 1;
	U1MODEbits.ON = 1;

#if uartOperationMode == 'I'
	IPC6bits.U1IP = 5; // definir prioridade. (default = 0 => não funciona)
	IEC0bits.U1RXIE = 1; // Enable Receção
	IEC0bits.U1TXIE = 1; // Enable Transmição

#endif

}

void putc(char byte2send){
	while(U1STAbits.UTXBF == 1);
	U1TXREG = byte2send;
}

void putS(char *s){
	char *p = s;
	while(*p != '\0'){
		putc(*p);
		p++;
	}
}

char getc(void){
	if (U1STAbits.OERR == 1)
		U1STAbits.OERR == 0;
	while(U1STAbits.URXDA == 0);
	return U1RXREG;
	
}



