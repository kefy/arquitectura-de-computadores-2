#ifndef	uartDriver_h
#define uartDriver_h

#include "detpic32.h"

#define uartOperationMode 		'P'


void configUart(unsigned int baudRate, char parity, char stopBits);
void putc(char byte2send);
void putS(char *s);
char getc();

#endif
