#include "I2C_detPic.h"

void i2c1_init(unsigned int clock_freq){
	// Cálculo da baudrate
	I2C1BRG = (PBCLK + clock_freq)/(2*clock_freq) - 1;
	// Activar o modulo I2C
	I2C1CONbits.ON = 1;
}

void i2c1_start(){
	// Activa condição de start
	I2C1CONbits.SEN = 1;
	//Espera que termine
	while(I2C1CONbits.SEN == 1);
}

void i2c1_stop(){
	while((I2C1CON & 0x1F) != 0);
	I2C1CONbits.PEN = 1;
	while(I2C1CONbits.PEN);
}

int i2c1_send(unsigned char value){
	I2C1TRN = value;
	while(I2C1STATbits.TRSTAT);
	return I2C1STATbits.ACKSTAT;
}

char i2c1_receive(char ack_bit){
	while((I2C1CON & 0x1F) != 0);
	I2C1CONbits.RCEN = 1;
	while(!I2C1STATbits.RBF);
	I2C1CONbits.ACKDT = (0x01 & ack_bit); // be sure ackbit value is only 0 or 1
	I2C1CONbits.ACKEN = 1;
	while(I2C1CONbits.ACKEN);
	printStr("here");

	return I2C1RCV;
}

void delay(unsigned int n_intervals){
	volatile unsigned int i;
	
	for(; n_intervals != 0 ; n_intervals--){
		for(i = CALIBRATION_VALUE; i != 0 ; i--);
	}
}





