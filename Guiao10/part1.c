#include "I2C_detPic.h"

int main()
{
	int ack, temperature;
	i2c1_init(TC74_CLK_FREQ);
	
	while(1){
		i2c1_start();
		ack = i2c1_send(ADDR_WR);
		ack+=  i2c1_send(RTR);
		i2c1_start();
		ack+=  i2c1_send(ADDR_RD);

		if (ack != 0){
			printStr("ERROR");
			i2c1_stop();
			break;
		}
		temperature = i2c1_receive(I2C_NACK);
		printStr("\ninit_ok");
		printInt10(temperature);	
		i2c1_stop();
		printStr("\nTemperatura: ");
		printInt10(temperature);	
		delay(250);	
	}
	
	printStr("\ninit_ok");

	return 0;
}

